package model.order;

import lombok.Data;

import java.util.Date;

@Data
public class Order {

    private String companyName;
    private String orderNumber;
    private Date date;
    private String address;
    private String fullName;
    private String bossName;

}
