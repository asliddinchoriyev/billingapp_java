package test;

import java.math.BigDecimal;
import java.util.List;

public class Merchant {

    private int id;
    private String name;
    private BigDecimal minAmount;

    List<Field> fields;

    public Merchant(int id, String name, BigDecimal minAmount, List<Field> fields) {
        this.id = id;
        this.name = name;
        this.minAmount = minAmount;
        this.fields = fields;
    }
}
