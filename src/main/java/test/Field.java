package test;

import java.util.List;

public class Field {

    private int id;
    private String name;
    private List<Value> values;

    public Field(int id, String name, List<Value> values) {
        this.id = id;
        this.name = name;
        this.values = values;
    }
}
