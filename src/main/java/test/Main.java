package test;

import model.order.Order;
import report.OrderReport;

import java.util.Date;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Order order = new Order();
        order.setCompanyName(scanner.nextLine());
        order.setOrderNumber(scanner.nextLine());
        order.setDate(new Date());
        order.setAddress(scanner.nextLine());
        order.setFullName(scanner.nextLine());
        order.setBossName(scanner.nextLine());
        OrderReport orderReport = new OrderReport();
        orderReport.getOrder(order);
    }
}
