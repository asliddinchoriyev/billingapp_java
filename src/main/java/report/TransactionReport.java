package report;

import com.google.gson.Gson;
import model.transaction.Transaction;
import model.transaction.TransactionState;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class TransactionReport {

    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public void getTransactionReport() {

        try {
            HSSFWorkbook workbook = new HSSFWorkbook(new FileInputStream("./report/transactions.xls"));
            HSSFSheet workbookSheet = workbook.getSheet("pdp bootcamp9");

            String arr[] = {"id", "account", "amount", "amount_in_currency",
                    "state", "gateway", "merchant", "created_date", "transaction_date"};

            HSSFRow row = workbookSheet.createRow(0);
            for (int i = 0; i < arr.length; i++) {
                row.createCell(i).setCellValue(arr[i]);
            }

            List<Transaction> transactionList = this.getTransactions();

            for (int i = 0; i < transactionList.size(); i++) {
                HSSFRow row1 = workbookSheet.createRow(i + 1);
                HSSFCell cell = row1.createCell(0);
                cell.setCellValue(transactionList.get(i).getId().toString());
                row1.createCell(1).setCellValue(transactionList.get(i).getTransactionAccount());
                row1.createCell(2).setCellValue(transactionList.get(i).getTransactionAmount().toString());
                row1.createCell(3).setCellValue(transactionList.get(i).getTransactionAmountCurrency().toString());
                row1.createCell(4).setCellValue(transactionList.get(i).getState());
                row1.createCell(5).setCellValue(transactionList.get(i).getGateway().getName());

                row1.createCell(6).setCellValue(transactionList.get(i).getMerchant().getName());
                row1.createCell(7).setCellValue(simpleDateFormat.format(transactionList.get(i).getCreatedDate()));
//            row1.createCell(8).setCellValue(simpleDateFormat.format(transactionList.get(i).getTransactionDate()));
            }


            FileOutputStream fileOutputStream = new FileOutputStream("./report/transactions.xls");
            workbook.write(fileOutputStream);
            fileOutputStream.close();
            workbook.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private List<Transaction> getTransactions() {
        List<Transaction> transactions = new ArrayList<>();
        try {
            FileReader fileReader = new FileReader("./transaction/2021-03-18/13.log");
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String line;

            while ((line = bufferedReader.readLine()) != null)
                transactions.add(new Gson().fromJson(line, Transaction.class));
            bufferedReader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        List<Transaction> successTransactions = transactions.stream().filter((transaction ->
                transaction.getStatedId() == TransactionState.CHECKED.getValue())).collect(Collectors.toList());

        return successTransactions;
    }
}
