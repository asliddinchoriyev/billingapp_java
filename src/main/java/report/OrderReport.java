package report;

import model.order.Order;
import org.apache.poi.POIDocument;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.xwpf.usermodel.XWPFDocument;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class OrderReport {

    public void getOrder(
            Order order
    ){
        try {
            XWPFDocument document = new XWPFDocument(OPCPackage.open("./report/buyruq.docx"  ));
            replaceTextFor(document,"","");
            document = replaceTextFor(document, "companyName", order.getCompanyName());
            document = replaceTextFor(document, "bossName", order.getBossName());
            document = replaceTextFor(document, "orderNumber", order.getOrderNumber());
            document = replaceTextFor(document, "Date", order.getDate().toString());
            document = replaceTextFor(document, "address", order.getAddress());
            document = replaceTextFor(document, "fullName", order.getFullName());
            saveWord("./report/buyruq1.docx", document);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private static XWPFDocument replaceTextFor(XWPFDocument doc, String findText, String replaceText){
        doc.getParagraphs().forEach(p ->{
            p.getRuns().forEach(run -> {
                String text = run.text();
                if(text.contains(findText)) {
                    run.setText(text.replace(findText, replaceText),0);
                }
            });
        });

        return doc;
    }

    private static void saveWord(String filePath, XWPFDocument doc) throws FileNotFoundException, IOException, IOException {
        FileOutputStream out = null;
        try{
            out = new FileOutputStream(filePath);
            doc.write(out);
        }
        catch(Exception e) {
            e.printStackTrace();
        }
        finally{
            out.close();
        }
    }
}
